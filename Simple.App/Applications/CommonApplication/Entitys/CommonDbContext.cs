﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simple.EntityFrameworkCore;
using Simple.CommonApplication.Entitys;

namespace Simple.CommonApplication
{
    public class CommonDbContext : AppDbContext
    {
        public CommonDbContext(DbContextOptions<CommonDbContext> options) : base(options)
        {
        }

        public DbSet<OperateLog> OperateLog { get; set; }
    }
}